import torch
import torch.optim as optim
import numpy as np

class SumTree:
  """efficient memory data sctructure class (fast retrieves and updates).

  source of the SumTree class code : https://github.com/jaromiru/AI-blog/blob/master/SumTree.py
  
  **Parameters**:

  *capacity* (*int*): number of tree leaves

  """
  write = 0
  current_size=0

  def __init__(self, capacity):
    # INPUT
    # *capacity*: number of tree leaves
    self.capacity = capacity
    self.tree = np.zeros( 2*capacity - 1 )
    self.data = np.zeros( capacity, dtype=object )

  def _propagate(self, idx, change):
    parent = (idx - 1) // 2

    self.tree[parent] += change

    if parent != 0:
      self._propagate(parent, change)

  def _retrieve(self, idx, s):
    left = 2 * idx + 1
    right = left + 1

    if left >= len(self.tree):
      return idx

    if s <= self.tree[left]:
      return self._retrieve(left, s)
    else:
      return self._retrieve(right, s-self.tree[left])

  def get_current_size(self):
    return self.current_size

  
  def total(self):
    """returns the sum of leaf weights
    """
    return self.tree[0]

  def add(self, p, data):
    """adds data to tree, potentially overwritting older data
  
    **Parameters**:

    *p* (*float*): leaf weight

    *data*: leaf data
    """
  
    idx = self.write + self.capacity - 1

    self.data[self.write] = data
    self.update(idx, p)

    self.write += 1
    if self.write >= self.capacity:
      self.write = 0

    self.current_size = min(self.current_size+1,self.capacity)

  def update(self, idx, p):
    """updates leaf weight
    
    **Parameters**:

    *idx* (*int*): leaf index
    
    *p* (*float*): new weight

    """
    change = p - self.tree[idx]

    self.tree[idx] = p
    
    if self.capacity > 1:
      self._propagate(idx, change)

  def get(self, s):
    """get leaf corresponding to numeric value
    
    **Parameters**:

    *s* (*float*): numeric value

    **Returns**:

    triplet with leaf id (*int*), tree node id (*int*) and  leaf data
    """

    idx = self._retrieve(0, s)
    dataIdx = idx - self.capacity + 1

    return (idx, self.tree[idx], self.data[dataIdx])






class BellmanQNetwork(object):

	def __init__(
		self,
		state_encoder,
		state_predictor,
		controller,env,
		max_memory_size=1000):

		self.state_encoder = state_encoder
		self.state_predictor = state_predictor
		self.controller = controller
		self.env = env
		self.memory = SumTree(max_memory_size)

	def train(
    self,
    n_episodes,
    max_ts_per_episodes,
    td_steps):

    memsize = 0
    td = 0 #temporal difference step counter

    for i in range(n_episodes):
      
      s1 = self.env.reset()
      ts_reward = 0

      td = 0
      step_list = deque(maxlen=td_steps)

      for j in range(max_ts_by_episode):

        #execute policy
        step_list.append(self._step(agent,s1,render))
        td += 1
        s1 = step_list[-1][3]
        r = step_list[-1][2] #latest reward

        if np.isnan(r):
          raise RuntimeError("The model diverged; decreasing step sizes or increasing tau can help to prevent this.")

        done = (step_list[-1][4] == 0)

        if td >= td_steps:
          # compute temporal difference n-steps SARST and its delta
          td_sarst = self._process_td_steps(step_list,discount_rate)
          delta = self._get_delta(agent,target,[td_sarst],discount_rate,torch.ones(1),td_steps,optimise=False)
          memory.add((delta[0] + 1.0/max_memory_size),td_sarst)

        # sgd update
        # get replay batch
        P = memory.total()
        N = memory.get_current_size()
        if N > 0:
          for h in range(steps_per_update):

            try:
              samples = np.random.uniform(high=P,size=min(scheduler.batch_size,N))
            except OverflowError as e:
              print(e)
              print("it seems that the model parameters are diverging. Decreasing step size or increasing tau might help.")

            batch = []
            batch_ids = []
            batch_p = []
            for u in samples:
              idx, p ,data = memory.get(u)
              batch.append(data) #data from selected leaf
              batch_ids.append(idx)
              batch_p.append(p/P)

            #compute importance sampling weights
            batch_w = np.array(batch_p)
            batch_w = (1.0/(N*batch_w))
            batch_w /= np.max(batch_w)
            batch_w = torch.from_numpy(batch_w).float()

            # perform optimisation
            delta = self._get_delta(agent,batch,discount_rate,batch_w,td_steps,optimise=True)

            #update memory
            for k in range(len(delta)):
              memory.update(batch_ids[k],(delta[k] + 1.0/max_memory_size)**scheduler.PER_alpha)

        # trace information
        ts_reward += r

        if done:
          break

      if td < td_steps:
        td_sarst = self._process_td_steps(step_list,discount_rate)
        delta = self._get_delta(agent,target,[td_sarst],discount_rate,torch.ones(1),td_steps,optimise=False)
        memory.add((delta[0] + 1.0/max_memory_size),td_sarst)

      self.mean_trace.append(ts_reward)
      logging.info("episode {n}, timestep {ts}, mean reward {x}".format(n=i,x=ts_reward,ts=scheduler.counter))

    if render:
      self.env.close()

	def _step(self,s1,render=False):
		# perform an action given an agent, a target, the current state, and an epsilon (exploration probability)
		with torch.no_grad():
		  q = self.controller.forward(s1)

		_, a = q.max(0) #argmax
		a = int(a.numpy())

		sarst = (s1,a) #t = termination

		s2,r,done,info = self.env.step(a)
		if render:
		  self.env.render()
		sarst += (r,s2,1-int(done)) #t = termination signal (t=0 if s2 is terminal state, t=1 otherwise)

		return sarst

  def _get_delta(
      self,
      batch,
      discount_rate,
      sample_weights,
      td_steps,
      optimise=True):

      #process batch
      S1 = torch.from_numpy(np.array([x[0] for x in batch])).float()
      A1 = torch.from_numpy(np.array([x[1] for x in batch])).long()
      R = torch.from_numpy(np.array([x[2] for x in batch])).float()
      S2 = torch.from_numpy(np.array([x[3] for x in batch])).float()
      T = torch.from_numpy(np.array([x[4] for x in batch])).float()

      # update state autoencoder
      self.state_encoder.step(S1)

      with torch.no_grad():
        s1 = self.state_encoder.encode(S1)
        s2 = self.state_encoder.encode(S2)
      
      a1 = self.one_hot_encoding(A1)
      # update system dynamics predictor with updated latent states
      self.state_predictor.step(torch.concat((s1,a1),0),torch.concat((s2,R),0))

      # sample next states
      mc_estimate = torch.zeros(R.shape)
      for i in range(n_samples):
        sample = self.state_predictor.sample(s1,a1)
        sample_state = sample[:,0:sample.shape[1]-1]
        sample_r = sample[:,sample.shape[1]]
        with torch.no_grad():
          sample_q = self.controller.forward(sample_state)
          max_sample_q, _ = torch.max(sample_q,0)
    
        mc_estimate += sample_r + self.gamma**(td_steps)*max_sample_q

      mc_estimate /= n_samples
      # update q-network
      self.controller.step(s1,R + mc_estimate)
      
      return torch.abs(delta).detach().numpy()
