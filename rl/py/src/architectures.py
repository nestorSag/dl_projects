import math
import pandas as pd
import numpy as np 
import copy

import torch
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F

from rlmodels.nets import FullyConnected

import matplotlib.pyplot as plt
import seaborn as sns


class TreeLevel(nn.Module):

	def __init__(self,input_size,rf_size):

		super(TreeLevel,self).__init__()

		residual = input_size%rf_size
		if residual == 0:
			self.padding = 0
		else:
			self.padding = rf_size - input_size%rf_size

		idx_list = np.arange(input_size)
		np.random.shuffle(idx_list)
		idx_list = np.concatenate((idx_list,np.arange(input_size,input_size+self.padding)))
		self.output_size = int(len(idx_list)/rf_size)

		self.subsets = []
		self.n_params = 0
		#print("padding {p}".format(p=self.padding))
		for i in range(self.output_size):
			subset = idx_list[(rf_size*i):((i+1)*(rf_size))]
			subset = subset[subset < input_size]
			self.subsets.append(subset)
			#print("subset: {s}".format(s=subset))
			setattr(self,"linear{n}".format(n=i),nn.Linear(len(subset),1))
			self.n_params += len(subset)+1

	def forward(self,x,debug=False):

		#m,n = x.shape
		m = 1 if len(x.shape) == 1 else x.shape[0]
		output = torch.empty((m,self.output_size))

		for i in range(self.output_size):
			if debug:
				print("i: {i},subset: {s}".format(i=i,s=self.subsets[i]))
			output[:,i] = getattr(self,"linear{n}".format(n=i))(x[:,self.subsets[i]]).view(-1)

		return F.sigmoid(output)

	def parameters(self):
		params = []
		for i in range(self.output_size):
			params += list(getattr(self,"linear{n}".format(n=i)).parameters())

		return params

class Tree(object):

	def __init__(self,input_size,rf_size,depth):

		if isinstance(rf_size,int):
			rf_sizes = [rf_size for i in range(depth)]

		self.levels = []
		self.n_params = 0
		level_output_size = input_size
		for i in range(depth):
			level = TreeLevel(level_output_size,rf_sizes[i])
			level_output_size = level.output_size
			self.levels.append(level)
			self.n_params += level.n_params

		self.output_size = self.levels[-1].output_size

	def parameters(self):
		params = []
		for level in self.levels:
			params += level.parameters()

		return params 

	def forward(self,x):

		output = x
		for level in self.levels:
			output = level.forward(output)

		return output

class ForestLayer(object):

	def __init__(self,input_size,n_trees,rf_size,depth):

		if isinstance(rf_size,int):
			rf_sizes = [rf_size for i in range(n_trees)]

		if isinstance(depth,int):
			depths = [depth for i in range(n_trees)]

		self.trees = []

		self.output_size = 0
		self.n_params = 0
		for i in range(n_trees):
			tree = Tree(input_size,rf_sizes[i],depths[i])
			self.trees.append(tree)
			self.output_size += tree.output_size
			self.n_params += tree.n_params

	def parameters(self):
		params = []
		for tree in self.trees:
			params += tree.parameters()

		return params

	def forward(self,x):

		m = 1 if len(x.shape) == 1 else x.shape[0]
		output = torch.empty((m,self.output_size))

		#print("output size: {o}".format(o=output.shape))
		#i = 0
		sliding_window = 0
		for tree in self.trees:
			#print("sliding window: {w}, i: {i}")
			output[:,sliding_window:(sliding_window+tree.output_size)] = tree.forward(x)
			sliding_window += tree.output_size
			#i += 1

		return output

class ForestNetwork(object):

	def __init__(
		self,
		input_size,
		output_size,
		n_trees_list,
		rf_size_list,
		depth_list,
		output_activation=None):

		self.layers = []
		self.output_activation = output_activation

		layer_output_size = input_size
		self.n_params = 0
		for i in range(len(n_trees_list)):
			layer = ForestLayer(
				input_size=layer_output_size,
				n_trees = n_trees_list[i],
				rf_size = rf_size_list[i],
				depth = depth_list[i])
			layer_output_size = layer.output_size
			self.layers.append(layer)
			self.n_params += layer.n_params

		self.output_layer = nn.Linear(layer_output_size,output_size)

	def parameters(self):
		params = []
		for layer in self.layers:
			params += layer.parameters()

		return params

	def forward(self,x):

		flatten_output = False
		if isinstance(x,np.ndarray):
			x = torch.Tensor(x)

		if len(x.shape) == 1:
			n = x.shape[0]
			x = x.view((1,n))

		output = x
		for layer in self.layers:
			output = layer.forward(output)

		output = self.output_layer(output)
		if self.output_activation is not None:
			output = self.output_activation(output)

		# if flatten_output:
		# 	if isinstance(output,np.ndarray):
		# 		output = output.reshape(-1)
		# 	else:
		# 		output = output.view(-1)

		return output

	def state_dict(self):

		state = {}
		layers_idx = range(len(self.layers))
		for layer_idx in layers_idx:
			trees_idx = range(len(self.layers[layer_idx].trees))
			for tree_idx in trees_idx:
				levels_idx = range(len(self.layers[layer_idx].trees[tree_idx].levels))
				for level_idx in levels_idx:
					neurons_idx = range(self.layers[layer_idx].trees[tree_idx].levels[level_idx].output_size)
					for neuron_idx in neurons_idx:
						key = "layer{a}_tree{b}_level{c}_neuron{d}".format(a=layer_idx,b=tree_idx,c=level_idx,d=neuron_idx)
						linear_layer = getattr(self.layers[layer_idx].trees[tree_idx].levels[level_idx],"linear{x}".format(x=neuron_idx))
						state[key + ".weight"] = linear_layer.state_dict()["weight"]
						state[key + ".bias"] = linear_layer.state_dict()["bias"]

		state["output_layer.weight"] = self.output_layer.state_dict()["weight"]
		state["output_layer.bias"] = self.output_layer.state_dict()["bias"]
		return state

	def load_state_dict(self,d):

		layers_idx = range(len(self.layers))
		for layer_idx in layers_idx:
			trees_idx = range(len(self.layers[layer_idx].trees))
			for tree_idx in trees_idx:
				levels_idx = range(len(self.layers[layer_idx].trees[tree_idx].levels))
				for level_idx in levels_idx:
					neurons_idx = range(self.layers[layer_idx].trees[tree_idx].levels[level_idx].output_size)
					for neuron_idx in neurons_idx:
						key = "layer{a}_tree{b}_level{c}_neuron{d}".format(a=layer_idx,b=tree_idx,c=level_idx,d=neuron_idx)
						linear_layer_dict = {"weight":d[key + ".weight"],"bias":d[key + ".bias"]}
						linear_layer = getattr(self.layers[layer_idx].trees[tree_idx].levels[level_idx],"linear{x}".format(x=neuron_idx))
						linear_layer.load_state_dict(linear_layer_dict)

		self.output_layer.load_state_dict({"weight":d["output_layer.weight"],"bias":d["output_layer.bias"]})




class ExpandedForest(object):

  def __init__(self,dense_layers,forest_network):
    self.dense = dense_layers
    self.forest = forest_network

  def forward(self,x):

  	if isinstance(x,np.ndarray):
  		x = torch.Tensor(x)
  		
  	return self.forest.forward(F.relu(self.dense.forward(x)))

  def parameters(self):

  	return list(self.dense.parameters()) + list(self.dense.parameters())


