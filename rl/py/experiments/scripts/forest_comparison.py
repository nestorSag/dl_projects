import numpy as np 
import torch
import torch.optim as optim
import gym

from rlmodels.models.DQN import *
from rlmodels.nets import FullyConnected

import logging

import sys
sys.path.append('rl/py/src')
import architectures as models

import torch.nn.functional as F

FORMAT = '%(asctime)-15s: %(message)s'
logging.basicConfig(level=logging.INFO,format=FORMAT,filename="model_fit.log",filemode="a")

env = gym.make('LunarLander-v2')
max_ep_ts = 1000

env.seed(1)
np.random.seed(1)
torch.manual_seed(1)

# agent output is argmax from a 2-dimensional output vector (values not related to Q function!) 
def forest_output(x):
  out = x.detach().numpy()
  if len(out.shape) == 2 and out.shape[0] == 1:
    out = out.reshape(-1)
  return out

# def vanilla_output(x):
#   out = F.tanh(x).detach().numpy()
#   if len(out.shape) == 2 and out.shape[0] == 1:
#     out = out.reshape(-1)
#   return out

#CMAES is not gradient-based, so we don't have to wrap the model in an Agent instance
dense_output = 125
dense = nn.Linear(8,dense_output)

forest = models.ForestNetwork(
  input_size=dense_output,
  output_size=4,
  n_trees_list = [10,10],
  rf_size_list=[5,5],
  depth_list=[2,2],
  output_activation=forest_output)

agent_lr = 0.01 #initial learning rate
agent_model = models.ExpandedForest(dense,forest)

# set hyperparameter runtime schedule as a function of the global number of timesteps
dqn_scheduler = DQNScheduler(
  batch_size = lambda t: 200 if t < 75000 else 300, #constant
  exploration_rate = lambda t: 0.05 if t < 75000 else 0.01, #decrease exploration down to 1% after 10,000 steps
  PER_alpha = lambda t: 1, #constant
  PER_beta = lambda t: 1, #constant
  tau = lambda t: 100, #constant
  agent_lr_scheduler_fn = lambda t: 1 if t < 75000 else 0.1, #decrease step size every 2,500 steps,
  steps_per_update = lambda t: 1) #constant

#agent_model = FullyConnected([200,200],8,4,None)
agent_opt = optim.SGD(agent_model.parameters(),lr=agent_lr,weight_decay = 0, momentum = 0)

agent = Agent(agent_model,agent_opt)

dqn = DQN(agent,env,dqn_scheduler)

dqn.fit(
  n_episodes=150,
  max_ts_by_episode=max_ep_ts,
  max_memory_size=2000,
  td_steps=5)

dqn.plot()
dqn.play()