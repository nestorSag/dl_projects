import torch
import torch.optim as optim

import sys
sys.path.append('rl/py/src')
import architectures as models

input_size = 100
x = torch.rand((1,input_size))
rf_size = 2
model = models.TreeLevel(input_size,rf_size)
model.forward(x)

depth = 4
model = models.Tree(input_size,rf_size,depth)
model.forward(x)

n_trees = 25
model = models.ForestLayer(input_size,n_trees,rf_size,depth)
model.forward(x)

n_layers = 2
output_size = 10
model = models.ForestNetwork(input_size,output_size,[25,10],[2,2],[4,4])
model.forward(x)


