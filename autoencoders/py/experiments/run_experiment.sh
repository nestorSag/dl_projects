#!/bin/bash
#this scrits uses dvc to track the experiment dependencies and results in a remote repository
preffix="autoencoders/py"
pipenv run dvc run -f $1 \
    -d $preffix/src/models.py -d $preffix/src/data_getters.py $preffix/experiments/run.py \
    -d autoencoders/test_data/ -d $preffix/experiments/config/config.json
    -o $preffix/experiments/results/ \
    'pipenv run python $preffix/experiments/run.py $preffix/experiments/config/config.json $preffix/experiments/results/'