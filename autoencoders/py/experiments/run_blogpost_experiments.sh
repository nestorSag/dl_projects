#!/bin/bash
preffix="autoencoders/py/experiments"

pipenv run python $preffix/data_setup.py

pipenv run python $preffix/run.py $preffix/configs/angular_reuters_WBN_ciclical.json $preffix/results/angular_reuters_WBN_ciclical/

pipenv run python $preffix/run.py $preffix/configs/angular_hand_posture_WBN_ciclical.json $preffix/results/angular_hand_posture_WBN_ciclical/

pipenv run python $preffix/run.py $preffix/configs/angular_reuters_WBN.json $preffix/results/angular_reuters_WBN/

pipenv run python $preffix/run.py $preffix/configs/cartesian_hand_posture_WBN.json $preffix/results/cartesian_hand_posture_WBN/

