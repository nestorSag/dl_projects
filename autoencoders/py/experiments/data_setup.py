import os
import dload
import tarfile

import nltk

if __name__ == "__main__":

	nltk.download("reuters")

	filepath = "autoencoders/test_data/hand_posture"
	url = "https://archive.ics.uci.edu/ml/machine-learning-databases/00405/Postures.zip"

	if not os.path.exists(filepath):
		os.makedirs(filepath)
		dload.save_unzip(url, filepath)

