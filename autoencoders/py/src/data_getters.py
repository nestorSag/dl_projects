import re
import os
import pandas as pd
import numpy as np 

from sklearn.feature_extraction.text import CountVectorizer
from nltk.stem import PorterStemmer 

from mnist import MNIST
from nltk.corpus import reuters
import scipy.sparse as sparse

DATA_FOLDER = "autoencoders/test_data/"

# each function returns a dataset with their vectors of mean and standard deviations

def get_mnist_data(n=5000,complete = False,simplify=True):
	# if complete is set to True, do not remove constant columns
	# if simplify, collapse image to 0 and 1 values
	mndata = MNIST(DATA_FOLDER + 'mnist')
	images, labels = mndata.load_training()
	#
	data = np.array(images)
	#
	if n is None:
		n = data.shape[0]
		#
	idx = np.random.choice(data.shape[0],min(n,data.shape[0]),False)
	data = data[idx,:]
	labels = np.array(labels)[idx]
	#
	# map to binary image
	if simplify:
		px = 250
		data[data < px] = 0
		data[data >= px] = 1
	#
	#drop constant columns
	sigma = np.std(data,axis=0)
	if complete:
		sigma[sigma == 0] = 1 #map 0 standard deviations to unity
	else:
		data = data[:,sigma > 0]
		sigma = np.std(data,axis=0)
		#
	mu = np.mean(data,axis=0)
	#
	return data, mu, sigma, labels

def get_CAE_mnist_data(n=5000):
	# get MNIST data and reshape vectors into pixel matrices
	data, mu, sigma, labels = get_mnist_data(n,True,False)
	m,n = data.shape
	k = int(np.sqrt(n))
	data = data.reshape((m,1,k,k))
	return data, mu, sigma, labels

def get_angular_conv_mnist_data(n=5000):
	# get MNIST data and reshape vectors into pixel matrices
	data, mu, sigma, labels = get_mnist_data(n,True,False)
	m,n = data.shape
	data = data.reshape((m,1,1,n))
	return data, mu, sigma, labels

def get_angular_mnist_data(n=5000):
	return get_mnist_data(n,True,False)

def get_hand_posture_data():
	df = pd.read_csv(DATA_FOLDER + "hand_posture/Postures.csv").\
	   replace(to_replace="?",value = np.nan).\
	   iloc[1::,0:35].\
	   dropna()
	class_ = [str(x) for x in df["Class"]]
	user = [str(x) for x in df["User"]]
	mat = np.array(df.iloc[:,3:35].astype(np.float64))
	mu = np.mean(mat,axis=0)
	stdev = np.std(mat,axis=0)
	#
	return mat, mu, stdev, class_, user



class StemmedCountVectorizer(CountVectorizer):
	# Count vectorizer that performs stemming too
	def build_analyzer(self):
		analyzer = super(StemmedCountVectorizer, self).build_analyzer()
		porter_stemmer = PorterStemmer()
		return lambda doc: ([porter_stemmer.stem(w) for w in analyzer(doc)])

def get_text_data(raw_texts, normalisation="l2"):
	# load data

	def is_useful(x):
		# useful feature: no numbers, no words of length 2 or less
		return len(x) > 2 and not bool(re.match(r"\d+",x))
	#

	# transform to document-term matirx
	cv = StemmedCountVectorizer(
		encoding = "ISO-8859-1",
		strip_accents="ascii",
		lowercase = True,
		stop_words = 'english',
		max_df = 0.9,
		min_df = 0.05)
	#
	mat = cv.fit_transform(raw_texts)
	# do some heuristic filtering: no numbers or words with less than 3 characters
	features = cv.get_feature_names()
	useful_features_idx = [i for i in range(len(features)) if is_useful(features[i])]
	useful_features = [f for f in features if is_useful(f)]
	#
	mat = mat[:,useful_features_idx]
	#
	n,m = mat.shape
	v = np.ones((n,1))
	u = np.ones((m,1))
	#
	#normalise rows by document length
	doc_lengths = mat.dot(u).reshape(-1)
	norms = np.sqrt(mat.dot(mat.T).diagonal())
	rescaled_counts = np.empty((len(mat.data),))
	mat = mat.tocoo()
	for i in range(len(mat.data)):
		if normalisation == "l1":
			rescaled_counts[i] = mat.data[i]/doc_lengths[mat.row[i]] #norm 1 standardisation
		elif normalisation == "l2":
			rescaled_counts[i] = mat.data[i]/norms[mat.row[i]] #norm 2 standardisation
		else:
			raise Exception("normalisation not understood")
	#
	mat = sparse.coo_matrix((rescaled_counts,(mat.row,mat.col)),shape=(n,m))
	mat = mat.tocsr()
	# now find column-wise mean and standard deviation
	mu = (mat.T.dot(v)/n).T
	#
	var = (np.square(mat - mu).T.dot(v)/n).T
	sigma = np.sqrt(var)
	return mat, mu, sigma, useful_features

# by default load the largest ~ 3,000 Reuters texts
def get_reuters_data(include_raw = False):
	txts = []

	for doc_id in reuters.fileids():
		if doc_id.startswith("train"):		
			txts.append((reuters.raw(doc_id),reuters.categories(doc_id)))
	#
	texts = [x[0] for x in txts]
	labels = [x[1] for x in txts]
	log_th = 6.5 #based on length histogram. 
	loglengths = [np.log(len(x)) for x in texts]
	filter_idx = [i for i in range(len(loglengths)) if loglengths[i] >= log_th]
	#
	labels = [labels[i] for i in filter_idx]
	texts = [texts[i] for i in filter_idx] #take right cluster of texts in histogram
	res = get_text_data(texts) + (labels,) 
	if include_raw:
		res += (texts,)
	return res

def get_legal_text_data():
	df = pd.read_csv(DATA_FOLDER + "legal_text/legal_transcripts.csv",encoding = "ISO-8859-1")
	texts = df["x"]
	return get_text_data(texts)

def get_gmm_data(k,samples_per_k,s):
	dim = 3
	n_samples = samples_per_k
	data = np.empty((n_samples*k,dim))
	#
	for i in range(k):
		mean = s*np.random.random((dim,)) - 0.5*np.ones((dim,))
		cov = np.eye(dim)
		data[(n_samples*i):(n_samples*(1+i))] = np.random.multivariate_normal(mean,cov,size=n_samples)
	#
	np.random.shuffle(data)
	data_mean = np.mean(data,axis=0)
	data_sdev = np.std(data,axis=0)
	#
	return data, data_mean, data_sdev

def get_gene_data(file= DATA_FOLDER + "genes/datExpr.csv",transpose=True):
	df = pd.read_csv(file,encoding="UTF-8")
	mat = np.array(df.drop('Unnamed: 0',axis=1))
	#
	if transpose:
		mat = mat.T
	data_mean = np.mean(mat,axis=0)
	data_sdev = np.std(mat,axis=0)
	return mat, data_mean, data_sdev