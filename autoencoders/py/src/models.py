import math
import pandas as pd
import numpy as np 
import copy

import torch
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F


import matplotlib.pyplot as plt
import seaborn as sns

from abc import ABC, abstractmethod

################# ENCODER/DECODER ARCHITECTURES





class FullyConnectedWBN(nn.Module):
  """Fully connected network as in the rlmodels package but with batch norm layers intertwined
  
  **Parameters**: 

  *layer_sizes* (*list* of *int*s): list with hidden layer sizes 

  *input_size* (*int*): input size 

  *output_size* (*int*): output size 

  *final_activation* (*function*): torch activation function for output layer. Can be `None`

  """


  def __init__(
  	self,
  	layer_sizes,
  	input_size,
  	output_size,
  	final_activation):

    super(FullyConnectedWBN,self).__init__()
    self.final_activation = final_activation
    self.n_hidden_layers = len(layer_sizes)
    layers = [input_size] + layer_sizes + [output_size]
    for layer in range(len(layers)-1):
        setattr(self,"linear{n}".format(n=layer),nn.Linear(layers[layer],layers[layer+1]))
        setattr(self,"batchnorm{n}".format(n=layer),nn.BatchNorm1d(num_features = layers[layer+1]))

  def forward(self,x):
    if isinstance(x,np.ndarray):
      x = torch.from_numpy(x).float()

    for layer in range(self.n_hidden_layers):
      x = getattr(self,"linear{n}".format(n=layer))(x)
      x = getattr(self,"batchnorm{n}".format(n=layer))(x)
      x = F.relu(x)
    
    x = getattr(self,"linear{n}".format(n=self.n_hidden_layers))(x)
    if self.final_activation is not None:
      x = self.final_activation(x)
    return x


class ConvolutionalEncoder(nn.Module):
	"""Convolutional encoder class with batch normalization layers intertwined
  
	  **Parameters**: 

	  *input_shape* (*list* of *int*s): input shape

	  *kernel_sizes* (*list*): list of two-dimensional kernel sizes, one for each convolutional layer

	  *strides* (*list*): list of two-dimensional stride sizes, one for each convolutional layer size 

	  *filter_channels* (*list*): list with number of channels per layer

	  *encoding_dim* (*function*): number of encoding units

	  *final_activation* (*function*): Optional activation for final (encoding) layer

	  *depthwise_conv* (*boolean*): whether to do depthwise convolution

	  """
	def __init__(
		self,
		input_shape,
		kernel_sizes,
		strides,
		filter_channels,
		encoding_dim,
		final_activation=None,
		depthwise_conv = True):

		super(ConvolutionalEncoder,self).__init__()

		self.final_activation = final_activation
		self.n_conv_layers = len(kernel_sizes)
		self.filter_channels = filter_channels

		j = 0
		output_size = None

		self.paddings = []
		self.output_sizes = []
		float_output_sizes = []
		while j < len(kernel_sizes):

			input_size = input_shape if output_size is None else output_size
			kernel_size = kernel_sizes[j]
			stride = strides[j] 
			filter_channel = filter_channels[j]
			#find padding such that no column is lost
			padding = [0,0] #[max(0, kernel_size[j] - input_size[1+j]) for j in range(2)]

			k = input_size[1] - kernel_size[0] + 2*padding[0]
			s = stride[0]

			while k%s != 0 and padding[0] < kernel_size[0]:
				
				padding[0] += 1
				k = input_size[1] - kernel_size[0] + 2*padding[0]

			k = input_size[2] - kernel_size[1] + 2*padding[1]
			s = stride[1]

			while k%s != 0 and padding[1] < kernel_size[1]:
				
				padding[1] += 1
				k = input_size[2] - kernel_size[1] + 2*padding[1]

			self.paddings.append(padding)

			output_size = [filter_channel] + [int(1 + (input_size[1+i] - kernel_size[i] + 2*padding[i])/stride[i]) for i in range(2)]
			self.output_sizes.append(output_size)

			float_output_size = [filter_channel] + [1 + (input_size[1+i] - kernel_size[i] + 2*padding[i])/stride[i] for i in range(2)]
			float_output_sizes.append(float_output_size)

			conv_layer = nn.Conv2d(
				in_channels = input_size[0],
				out_channels = filter_channel,
				kernel_size = kernel_size,
				stride = stride,
				padding = padding,
				groups = min(filter_channel,input_size[0]) if depthwise_conv else 1)
			
			setattr(self,"conv{n}".format(n=j),conv_layer)

			batchnorm_layer = nn.BatchNorm2d(
				num_features = filter_channel)
			
			setattr(self,"batchnorm{n}".format(n=j),batchnorm_layer)

			j += 1

		# keep track of dropped columns
		#self.dropped_columns = [int(self.output_sizes[i] == float_output_sizes[i]) for i in range(len(self.output_sizes))]

		# make encoding layer: flatten convolutional layer and project to encoding dimension
		self.flattened_dim = int(np.prod(output_size))
		#print("flattened: {x}, encoding: {y}".format(x=self.flattened_dim,y=encoding_dim))
		setattr(self,"linear{n}".format(n=j),nn.Linear(self.flattened_dim,encoding_dim))

		batchnorm_layer = nn.BatchNorm1d(
				num_features = encoding_dim)
			
		setattr(self,"batchnorm{n}".format(n=j),batchnorm_layer)

	def forward(self,x):

		m = x.shape[0]
		#self.shape_trace = []

		if isinstance(x,np.ndarray):
		  x = torch.from_numpy(x).float()

		#self.shape_trace.append(x.shape)
		for n_layer in range(self.n_conv_layers):
			# convolution
			x = getattr(self,"conv{n}".format(n=n_layer))(x)
			# batch normalization
			x = getattr(self,"batchnorm{n}".format(n=n_layer))(x)
			x = F.relu(x)
			#self.shape_trace.append(x.shape)

		# flattened convolution
		x = x.view(m,self.flattened_dim)
		# encode 
		x = getattr(self,"linear{n}".format(n=self.n_conv_layers))(x)
		# normalise 
		x = getattr(self,"batchnorm{n}".format(n=self.n_conv_layers))(x)
		# activation optional
		if self.final_activation is not None:
		  x = self.final_activation(x)

		#self.shape_trace.append(x.shape)
		return x


class ConvolutionalDecoder(nn.Module):

	"""Convolutional decoder class with batch normalization layers intertwined
  
	  **Parameters**: 

	  *flat_input_dim* (*list* of *int*s): size of input when flattened

	  *shapes* (*list* of *int*s): list of shapes resulting from layer-wise transformation

	  *kernel_sizes* (*list*): list of two-dimensional kernel sizes, one for each convolutional layer

	  *strides* (*list*): list of two-dimensional stride sizes, one for each convolutional layer size 

	  *paddings* (*list*): list of paddings that were used in the encoder; the decoder will add the necessary paddings to recover original shapes layer by layer

	  *final_activation* (*function*): Optional activation for final (encoding) layer

	  *depthwise_conv* (*boolean*): whether to do depthwise convolution

	  """
	def __init__(
		self,
		flat_input_dim,
		shapes,
		kernel_sizes,
		strides,
		paddings,
		final_activation=None,
		depthwise_conv = True):

		super(ConvolutionalDecoder,self).__init__()

		self.final_activation = final_activation
		self.n_conv_layers = len(kernel_sizes)
		self.input_shape = shapes[0]

		output_size = None

		j = 0
		setattr(self,"linear{n}".format(n=j),nn.Linear(flat_input_dim,int(np.prod(shapes[0]))))

		batchnorm_layer = nn.BatchNorm2d(
			num_features = shapes[0][0])
			
		setattr(self,"batchnorm{n}".format(n=j),batchnorm_layer)

		self.output_sizes = []
		while j < len(kernel_sizes):

			input_size = shapes[0] if output_size is None else output_size
			kernel_size = kernel_sizes[j]
			stride = strides[j]
			padding = paddings[j]
			output_size = shapes[j+1]

			output_padding = [max(0,output_size[1+i] - ((input_size[1+i]-1)*stride[i] - 2*padding[i] + kernel_size[i])) for i in range(2)]

			self.output_sizes.append(output_size)

			layer = nn.ConvTranspose2d(
				in_channels = input_size[0],
				out_channels = output_size[0],
				kernel_size = kernel_size,
				stride = stride,
				padding = padding,
				output_padding = output_padding,
				groups = min(output_size[0],input_size[0]) if depthwise_conv else 1)

			setattr(self,"conv{n}".format(n=j+1),layer)

			batchnorm_layer = nn.BatchNorm2d(
				num_features = output_size[0])
			
			setattr(self,"batchnorm{n}".format(n=j+1),batchnorm_layer)
			

			j += 1

	def forward(self,x):

		m = x.shape[0]

		#self.shape_trace = []

		if isinstance(x,np.ndarray):
		  x = torch.from_numpy(x).float()
		#self.shape_trace.append(x.shape)

		# expand encoding
		x = getattr(self,"linear0")(x)
		# fold into convolutional shape
		x = x.view([m] + self.input_shape)
		# normalise
		x = getattr(self,"batchnorm0")(x)
		# non-linearity
		x = F.relu(x)

		# for every subsequent convolutional layer
		for n_layer in range(1,self.n_conv_layers):
			# convolution
			x = getattr(self,"conv{n}".format(n=n_layer))(x)
			# batch normalisation
			x = getattr(self,"batchnorm{n}".format(n=n_layer))(x)
			# non-linearity
			x = F.relu(x)
			#self.shape_trace.append(x.shape)

		#output (no batch norm)
		# activation optional
		x = getattr(self,"conv{n}".format(n=self.n_conv_layers))(x)
		if self.final_activation is not None:
			x = self.final_activation(x)

		return x















############### AUTOENCODER ARCHITECTURES

class BaseAutoencoder(ABC):
	"""Base autoencoder abstract class with basic necessary functionality;
  
	  **Parameters**: 

	  *encoder* (*nn.Module*): Encoder model

	  *decoder* (*nn.Module*): Decoder model

	  """

	def __init__(self,encoder,decoder):
		self.encoder = encoder
		self.decoder = decoder
		self.loss_trace = []
		#
	def eval(self):
		self.encoder.eval()
		self.decoder.eval()
	def train(self):
		self.encoder.train()
		self.decoder.train()

	def parameters(self):
		return list(self.encoder.parameters()) + list(self.decoder.parameters())
		#
	@abstractmethod
	def forward(self,data):
		#
		raise NotImplementedError("Base class does not implement a forward() method")
		#
	@abstractmethod
	def _custom_mapper(self,x,mu,sigma):
		# any preprocessing that needs to be done before passing the data through the model
		raise NotImplementedError("Base class does not implement a preprocessing mapper")
	#
	def normalize_batch(self,x,mu,sigma):
		return torch.Tensor((x-mu)/sigma)
	#
	def encode(self,data,mu,sigma):
		"""
		Pass data through encoder to project it in a lower dimensional space

		Parameters:

		data (torch.Tensor or np.array): data to pass through encoder
		
		mu (torch.Tensor or np.array): column-wise mean
		
		sigma (torch.Tensor or np.array): column-wise standard deviation

		"""
		self.eval()
		out = self.encoder.forward(self._custom_mapper(data,mu,sigma))
		self.train()
		return out
	#
	@abstractmethod
	def _loss(self,batch):
		raise NotImplementedError("Base class does not implement a loss function")
	#
	def fit(
		self,
		opt,
		data,
		mu,
		sigma,
		batch_size,
		epochs,
		scheduler = None,
		normalize_all=False,
		debug=False):
		
		"""
		Fit model
		
		Parameters:
		
		opt (torch.optimizer): optimizer object
		
		data (torch.Tensor or np.array): data to pass through encoder
		
		mu (torch.Tensor or np.array): object to substract when standardization

		sigma (torch.Tensor or np.array): object to divide by when standardizing
		
		batch_size (int>0): mini-batch size
		
		epochs (int > 0): number of epochs to train the model for
		
		scheduler (torch scheduler): learning rate scheduler
		
		normalize_all (logical): Should all the data be normalised at once, or should each mini-batch be normalized individually?
		it might be more convenient to normalize each mini-batch only, if the data is sparse and high-dimensional
		
		debug (logical): if True, print batch loss before and after SGD step
		"""

		data_size = data.shape[0]
		steps_per_epoch = int(data_size/batch_size)
		print("Running {x} SGD steps...".format(x=steps_per_epoch*epochs))

		if normalize_all:
			print("Normalizing all...")
			norm_data = self._custom_mapper(data,mu,sigma)

		for i in range(steps_per_epoch*epochs):
			opt.zero_grad()
			#
			batch_idx = np.random.choice(data_size,size=batch_size)

			if normalize_all:
				batch = norm_data[batch_idx,:]
			else:
				batch = self._custom_mapper(data[batch_idx,:],mu,sigma)
			#batch = data[batch_idx,:]
			#batch = get_sparse_slice(data[batch_idx,:].tocoo())
			#
			s1 = self._loss(batch)
			s1.backward()
			opt.step()

			if debug:
				with torch.no_grad():
					print("batch loss before step: {x}".format(x=s1))
					s2 = self._loss(batch)
					print("batch loss after step:  {x}".format(x=s2))

			loss = s1.detach().numpy()
			if i%steps_per_epoch == 0:
				print("iter: {it}".format(it=i/steps_per_epoch))
				self.loss_trace.append(float(loss))
				#self.lr.append(opt.param_groups[0]["lr"])
				if scheduler is not None:
					scheduler.step()
	#
	def plot_loss(self,save_to=None,show=True):
		"""
		Plot loss trace
		
		Parameters:

		save_to (string): file name to save it to
		
		show (logical): should the plot be displayed or only saved?

		"""
		# first value is always too large, so discard it

		sns.lineplot(range(len(self.loss_trace[2::])),self.loss_trace[2::])

		if save_to is not  None:
			plt.savefig(save_to)
		
		if show:
			plt.show()

		plt.close()
	#

	def project(self,x,mu,sigma):

		"""
		Returns encoded observations
		
		Parameters:

		x (torch.Tensor or np.array): data to pass through encoder
		
		mu (torch.Tensor or np.array): column-wise mean
		
		sigma (torch.Tensor or np.array): column-wise standard deviation

		"""
		m = x.shape[0]

		encoded = self.encode(x,mu,sigma).detach().numpy().reshape(m,-1)
		
		return encoded
	#
	@abstractmethod
	def _reconstruction_loss(self,batch):
		raise Exception("Reconstruction loss not implemented in base class")

class Autoencoder(BaseAutoencoder):
	#### Usual auto-encoder implementation
	def _custom_mapper(self,x,mu,sigma):
		# any preprocessing that needs to be done before passing the data through the model
		return self.normalize_batch(x,mu,sigma)

	def _loss(self,batch):
		return self._reconstruction_loss(batch)

	def forward(self,data):
		#
		return self.decoder.forward(self.encoder.forward(data))

	def _reconstruction_loss(self,batch):
		batch_hat = self.forward(batch)
		rloss = ((batch - batch_hat)**2).mean()
		return rloss


class AngularAutoencoder(BaseAutoencoder):
	"""Angular autoencoder class; it works with spherical coordinates and encodes either angular features or both angular and norm features
  
	  **Parameters**: 

	  *encoder* (*nn.Module*): encoder model

	  *decoder* (*nn.Module*): encoder model

	  *enforce_angle_encoding* (*boolean*): If set to True, norm and/or angle space restriction are enforced in the encoded features 

	  *closedness_weight* (*int*): If enforce_angle_encoding is set to True, weight with which closedness (periodicity) in last angular dimension is enforced at fitting time

	  *encode_norm* (*boolean*): If set to True, encode norm as well

	"""

	def __init__(
		self,
		encoder,
		decoder,
		enforce_angle_encoding = True,
		closedness_weight = 1,
		encode_norm = False):

		super(AngularAutoencoder,self).__init__(encoder,decoder)
		self.closed_path_loss = []
		self.closedness_weight = int(closedness_weight)
		self.bottleneck_size = None
		self.enforce_angle_encoding = enforce_angle_encoding
		self.encode_norm = encode_norm

		if self.encode_norm:
			self.norm_loss = []

		self.decoder_take_norms = True #if norms are encoded, they are decoded as well

	def _angle_activation(self,x):
		# enforces angular domain restrictions
		angle = np.pi * torch.sigmoid(x)
		angle[:,-1] *= 2
		return angle

	def _norm_activation(self,x):

		# custom norm activation function
		#  currently linearized exponential function
		y = x
		y[y<=0] = torch.exp(y[y<=0])
		y[y>0] = 1 + y[y>0]
		return y

		# return nn.soft_relu(x)
		# return torch.log(1 + torch.exp(x))

		# y[torch.abs(y) >= 1] = torch.abs(y[torch.abs(y) >= 1]) - 0.5
		# y[torch.abs(y) < 1] = 0.5*(y[torch.abs(y) < 1])**2
		# return y

	def _cartesian_to_angular(self,x):
		# take data in usual Cartesian coordinates and return angular representation
		_,n = x.shape
		rev_col_idx = list(reversed(range(n)))
		cumnorms = torch.sqrt(torch.cumsum((x**2)[:,rev_col_idx],dim=-1)[:,rev_col_idx])
		std_batch = torch.acos(x/cumnorms)
		std_batch[std_batch != std_batch] = 0 #NaN are zeros by convention
		std_batch[x[:,n-1] < 0,n-2] = 2*np.pi - std_batch[x[:,n-1] < 0,n-2]

		if self.encode_norm:
			return torch.cat((self._euclidean_norm(x),std_batch[:,0:n-1]),1)
		else:
			return std_batch[:,0:n-1]
		#

	def _angular_to_cartesian(self,x):
		# take data in angular representation and return unit-norm Cartesian representations

		# since angular spaces involves long products of trigonometric functions, this method
		# handles everything in log scale to have long sums instead of long products, for higher 
		# numerical accuracy.
		
		if self.encode_norm:
			y = x[:,1:x.shape[1]]
			norms = x[:,0].view(x.shape[0],1)
		else:
			y = x
			norms = 1

		m,n = y.shape
		X = torch.cat((np.pi/2*torch.ones(m,1),y,torch.zeros((m,1))),dim=-1) #extended angle
		cos_ = torch.cos(X)
		sin_ = torch.sin(X)
		#need to keep track of the signs and then operate everything in absolute value
		# to be able to use logarithm sums
		cos_sign = torch.sign(cos_) + (cos_==0)
		sin_sign = torch.sign(sin_) + (sin_==0)
		cos_sign[torch.abs(cos_) < 1e-7] = 1 #collapse to zero (sign 1) when value is close enough
		sin_sign[torch.abs(sin_) < 1e-7] = 1 #collapse to zero (sign 1) when value is close enough
		angle_logcos = torch.log(torch.abs(cos_))
		angle_logsin = torch.log(torch.abs(sin_))
		cum_logsin = torch.cumsum(angle_logsin,dim=-1)
		log_cartesian = cum_logsin[:,0:(n+1)] + angle_logcos[:,1::]
		#operate the signs
		cum_logsin_sign = torch.cumprod(sin_sign,dim=-1)
		cartesian_signs = cum_logsin_sign[:,0:(n+1)]*cos_sign[:,1::]
		return norms*torch.exp(log_cartesian)*cartesian_signs

	#
	def _custom_mapper(self,x,mu,sigma):
		# normalise data, then get angular representation
		return self._cartesian_to_angular(self.normalize_batch(x,mu,sigma))

	def _closedness_loss(self,prepend=None):
		# optional arguments to append to the left of random angles, in case norm is being encoded
		####### add artificial term to loss to ensure encoding results is cyclic in last encoding dim

		# generate a uniformly distributed random angle
		n_ra = self.closedness_weight

		ra_ncol = self.bottleneck_size-1 if not self.encode_norm else self.bottleneck_size-2

		ra = np.pi*torch.rand((n_ra,ra_ncol))
		# append cyclic dimension
		cyclic_right_end = 2*np.pi*torch.ones((n_ra,1))
		cyclic_left_end = torch.zeros((n_ra,1))

		ra_right = torch.cat((ra,cyclic_right_end),1)
		ra_left = torch.cat((ra,cyclic_left_end),1)

		if prepend is not None:
			# append random norm columns to match decoder input dimension if needed
			ra_right = torch.cat((prepend,ra_right),1)
			ra_left = torch.cat((prepend,ra_left),1)
		# randomly decide which end is chasing the other end
		if float(np.random.uniform(size=1)>0.5):
			with torch.no_grad():
				r1 = self.decoder.forward(torch.Tensor(ra_right))
			r0 =  self.decoder.forward(torch.Tensor(ra_left))
		else:
			with torch.no_grad():
				r0 = self.decoder.forward(torch.Tensor(ra_left))
			r1 = self.decoder.forward(torch.Tensor(ra_right))
		##########

		cpl = F.smooth_l1_loss(r0, r1)
		
		return cpl

	def _reconstruction_loss(self,batch):

		batch_hat = self.forward(batch)

		return F.smooth_l1_loss(batch_hat, batch)
	
	def _loss(self,batch):

		m = batch.shape[0]

		r_loss =  self._reconstruction_loss(batch)#reconstruction loss
		# enforce closedness restriction in decoder in evaluation mode 
		if self.enforce_angle_encoding and self.closedness_weight > 0:
			if self.encode_norm and self.decoder_take_norms:
				avg_batch_norm = torch.mean(self._euclidean_norm(batch))
				sd_batch_norm = torch.std(self._euclidean_norm(batch))
				prepend = torch.randn((self.closedness_weight,1))*sd_batch_norm + avg_batch_norm
			else:
				prepend = None

			self.decoder.eval()
			c_loss = self._closedness_loss(prepend) #closedness loss
			self.decoder.train()
			self.closed_path_loss.append(float(c_loss.detach().numpy()))

			return r_loss + c_loss/m
		else:
			return r_loss

	def forward(self,data):
		#
		encoded = self.encoder.forward(data)

		if self.bottleneck_size is None:
			self.bottleneck_size = encoded.shape[1]
			if self.encode_norm and self.bottleneck_size == 1:
				raise Exception("Not enough units to encode angle space")

		if self.enforce_angle_encoding:

			if self.encode_norm:
				encoded = self._enforce_spherical_restrictions(encoded)
				return self._enforce_spherical_restrictions(self.decoder.forward(encoded))
			else:
				encoded = self._angle_activation(encoded)
				return self._angle_activation(self.decoder.forward(encoded))

		else:
			decoded = self.decoder.forward(encoded)
			if self.encode_norm:
				return self._enforce_spherical_restrictions(decoded)
			else:
				return self._angle_activation(decoded)
			
	def _enforce_spherical_restrictions(self,x):
		return torch.cat((self._norm_activation(x[:,0]).view(x.shape[0],1),self._angle_activation(x[:,1:x.shape[1]])),1)

	def plot_closedness_loss(self):
		# plot loss trace of path closedness error
		sns.lineplot(range(len(self.closed_path_loss)),self.closed_path_loss)
		plt.show()

	def project(self,x,mu,sigma):
		"""
		Project data in encoding space
		
		Parameters:

		x (torch.Tensor or np.array): data to pass through encoder
		
		mu (torch.Tensor or np.array): column-wise mean
		
		sigma (torch.Tensor or np.array): column-wise standard deviation

		"""

		with torch.no_grad():
			projection = self.encode(x,mu,sigma)

			if self.enforce_angle_encoding:

				if self.encode_norm:
					projection = self._angular_to_cartesian(self._enforce_spherical_restrictions(projection))

				else:
					print("Using norms of standardized data as projection norms")
					unit_norm = self._angular_to_cartesian(self._angle_activation(projection))
					norms = self._euclidean_norm(self.normalize_batch(x,mu,sigma))
					projection = norms * unit_norm
			
			return projection.detach().numpy()

	def _euclidean_norm(self,x):
		# unfold along first dimension
		return torch.norm(torch.Tensor(x).view(x.shape[0],-1),dim=-1).view(x.shape[0],1)


class AdditiveAngularAutoencoder(AngularAutoencoder):
	# In this architecture, the decoder only takes angular features, 
	# but can output both angular and norm features
	# Additionally, the norm unit from the encoding layer is directly added to the output norm unit
	def __init__(self,*args,**kwargs):
		super().__init__(*args,**kwargs)
		self.decoder_take_norms = False

	def forward(self,data):
		#
		encoded = self.encoder.forward(data)

		if self.bottleneck_size is None:
			self.bottleneck_size = encoded.shape[1]
			if self.encode_norm and self.bottleneck_size == 1:
				raise Exception("Not enough units to encode angle space")

		if self.enforce_angle_encoding:

			if self.encode_norm:
				norm = encoded[:,0]#.view(encoded.shape[0],1)
				angles = self._angle_activation(encoded[:,1:encoded.shape[1]]).view(encoded.shape[0],self.bottleneck_size-1)
				decoded = self.decoder.forward(angles)
				return self._add_to_output_channels(decoded,norm)
			else:
				encoded = self._angle_activation(encoded)
				return self._angle_activation(self.decoder.forward(encoded))

		else:
			decoded = self.decoder.forward(encoded)
			if self.encode_norm:
				return self._enforce_spherical_restrictions(decoded)
			else:
				return self._angle_activation(decoded)

	def _add_to_output_channels(self,decoded,norm):
		s = decoded.shape
		if len(s) == 2:
			decoded[:,0] += norm
			return decoded
		elif len(s) == 4:
			# assuming second dimensions are input channels
			n_obs = s[0]
			n_channels = s[1]
			#unfold channels
			decoded = decoded.view(n_obs,n_channels,-1)
			for i in range(n_channels):
				# add norm to first unit of each channel
				decoded[:,i,0] += norm
			# fold back
			decoded = decoded.view(s)
			return decoded
		else:
			raise Exception("You need to implement how to map encoded norms to output for this kind of data")

class ConvolutionalAngularAutoencoder(AngularAutoencoder):
	# this class accounts for the fact that multiple channels might be present in the input
	# each channel is processed separately, in effect getting multiple angular/spherical channels
	# as output 

	# This class assumes also that each channel has a dimension of 1 x L where 
	# L is the length of the unfolded image.
	# This is the only shape that makes sense in order to apply convolutional layers to angular data
	# due to the geometry of spherical transformations, which 'unfold' spatial statistical dependencies in 
	# Cartesian data to sequential dependence in the unfolded angular/spherical vector.

	def _cartesian_to_angular(self,x):

		return self._apply_channelwise_transform(x,super()._cartesian_to_angular)

	def _angular_to_cartesian(self,x):

		if len(x.shape) == 2:
			return super()._angular_to_cartesian(x)
		else:
			return self._apply_channelwise_transform(x,super()._angular_to_cartesian)
		#

	def _apply_channelwise_transform(self,x,transform):

		input_shape = x.shape
		n_obs = input_shape[0]
		n_channels = input_shape[1]
		#unfold each channel
		y = x.view(n_obs,n_channels,-1)
		# operate each channel separately and stack them
		y = torch.stack([transform(y[:,i,:]) for i in range(n_channels)], dim=1)
		# fold
		y = y.view((n_obs,n_channels,1,-1)) 
		return y

class VariationalBase(BaseAutoencoder):

	"""
	Adds variational functionality to an autoencoder

	Parameters:

	"""
	def __init__(self,encoder,decoder,ll_factor=1,**kwargs):

		super(VariationalBase,self).__init__(encoder,decoder,**kwargs)
		self.ll_factor = ll_factor

		self.ll_loss = []
		self.kl_loss = []

		encoding_dim = None

	def plot_loss(self,ll_loss=True,save_to=None,show=True):
		"""
		Plot loss trace
		
		Parameters:

		
		save_t (string): file name to save it to
		
		show (logical): should the plot be displayed or only saved?

		"""
		if ll_loss:
			sns.lineplot(range(len(self.ll_trace)),self.ll_trace)
		else:
			sns.lineplot(range(len(self.kl_trace)),self.kl_trace)

		if save_to is not  None:
			plt.savefig(save_to)
		
		if show:
			plt.show()

		plt.close()
	#

	def _kl_loss(self):

		return 0.5*(torch.mean(torch.exp(2*self.batch_log_sigma)) +\
		  torch.mean(self.batch_mu*self.batch_mu) - torch.mean(2*self.batch_log_sigma))

	def _loss(self,batch):
		#batch_hat = self.forward(batch)

		#ll_loss = ((batch_hat - batch)**2/(2*self.sigma**2)).mean() #loglikelihood
		
		ll_loss = super()._loss(batch) #underlying autoencoder error

		self.ll_trace.append(float(ll_loss))

		kl_loss = self._kl_loss()
		
		self.kl_trace.append(float(kl_loss))
		
		return self.ll_factor*ll_loss + kl_loss


class VariationalAutoencoder(VariationalBase, Autoencoder):

	# UNTESTED
	def forward(self,data):
		#
		encoded = self.encoder.forward(data)
		m,n = encoded.shape

		if self.encoding_dim is None:
			encoding_dim = int(n/2)
			if encoding_dim != n/2:
				raise Exception("Encoding dimension is not even; can't split in mean and variance")
			else:
				self.encoding_dim = encoding_dim

		mu = encoded[:,0:self.encoding_dim]
		log_sigma = encoded[:,self.encoding_dim:2*self.encoding_dim]

		z = torch.randn(size=(m,self.encoding_dim))

		self.batch_mu = mu
		self.batch_log_sigma = log_sigma
		return self.decoder.forward(torch.exp(self.batch_log_sigma)*z + self.batch_mu)

	def sample(self,n=1):
		z = torch.randn(size=(n,self.encoding_dim))
		return self.decoder.forward(z)

class VariationalAngularAutoencoder(VariationalBase,AngularAutoencoder):

	# UNTESTED
	def forward(self,data):
		#
		encoded = self.encoder.forward(data)
		m,n = encoded.shape

		if self.encoding_dim is None:
			encoding_dim = int(n/2)
			if encoding_dim != n/2:
				raise Exception("Encoding dimension is not even; can't split in mean and variance")
			else:
				self.encoding_dim = encoding_dim

		mu = encoded[:,0:self.encoding_dim]
		log_sigma = encoded[:,self.encoding_dim:2*self.encoding_dim]

		z = torch.randn(size=(m,self.encoding_dim))

		self.batch_mu = mu
		self.batch_log_sigma = log_sigma

		stochastic_input = torch.exp(self.batch_log_sigma)*z + self.batch_mu
		if self.enforce_angle_encoding:
			stochastic_input = self._angle_activation(stochastic_input)

		return self._angle_activation(self.decoder.forward(stochastic_input))

	def sample(self,n=1):
		z = torch.randn(size=(n,self.encoding_dim))
		return self._angular_to_cartesian(self.decoder.forward(z))