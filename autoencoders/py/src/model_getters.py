import models as models
from rlmodels.nets import FullyConnected

########### FACTORY METHODS FOR BASE MODELS
def do_size_arithmetic(
	input_size,
	encoding_dim,
	autoencoder_class,
	**kwargs):

	encoder_input_size = input_size
	encoder_output_size = encoding_dim

	decoder_input_size = encoding_dim
	decoder_output_size = input_size

	model_class = getattr(models,autoencoder_class)

	encode_norm_flag = "encode_norm" in kwargs.keys() and kwargs["encode_norm"]
	#enforce_restrictions_flag = "enforce_angle_encoding" in kwargs.keys() and kwargs["enforce_angle_encoding"]
	if issubclass(model_class,(models.AngularAutoencoder,models.AdditiveAngularAutoencoder)):
		if not encode_norm_flag:
			# angular space is n-1 dimensional compared to Cartesian coordinates

			#if we are dealing with convolutional data:
			if isinstance(input_size,tuple):
				encoder_input_size = list(encoder_input_size)
				decoder_output_size = list(decoder_output_size)

				encoder_input_size[-1] -= 1
				decoder_output_size[-1] -= 1

				encoder_input_size = tuple(encoder_input_size)
				decoder_output_size = tuple(decoder_output_size)
			else:
				encoder_input_size -= 1
				decoder_output_size -= 1

	# # the decoder in a AdditiveAngularAutoencoder instance only decodes angles
	if issubclass(model_class,models.AdditiveAngularAutoencoder):
		if encode_norm_flag_size:

			#if we are dealing with convolutional data:
			if isinstance(input_size,tuple):
				decoder_input_size = list(decoder_input_size)
				decoder_input_size[-1] -= 1
				decoder_input_size = tuple(decoder_input_size)
			else:
				decoder_input_size -= 1

	return encoder_input_size, encoder_output_size, decoder_input_size, decoder_output_size

def getFullyConnectedAutoencoder(
	input_size,
	layer_size,
	bottleneck_size,
	bottleneck_activation=None,
	final_activation=None,
	autoencoder_class="Autoencoder",
	batch_norm = False,
	**kwargs):

	architecture = models.FullyConnectedWBN if batch_norm else FullyConnected

	model_class = getattr(models,autoencoder_class)

	packed = do_size_arithmetic(input_size,bottleneck_size,autoencoder_class,**kwargs)
	encoder_input_size, encoder_output_size, decoder_input_size, decoder_output_size = packed

	encoder = architecture(layer_size,encoder_input_size,encoder_output_size,bottleneck_activation)
	decoder = architecture(list(reversed(layer_size)),decoder_input_size,decoder_output_size,final_activation)

	return model_class(encoder,decoder,**kwargs)

def getConvolutionalAutoencoder(
	input_size,
	kernel_sizes,
	strides,
	filter_channels,
	encoding_dim,
	autoencoder_class="Autoencoder",
	bottleneck_activation = None,
	final_activation=None,
	depthwise_conv = False,
	**kwargs):
	
	packed = do_size_arithmetic(input_size,encoding_dim,autoencoder_class,**kwargs)
	encoder_input_size, encoder_output_size, decoder_input_size, decoder_output_size = packed

	model_class = getattr(models,autoencoder_class)

	def _make_2d(l,model_class):
		if issubclass(model_class,(models.AngularAutoencoder,models.AdditiveAngularAutoencoder)):
			return [x if isinstance(x,list) and len(x) == 2 else [1,x[0]] if isinstance(x,list) else [1,x] for x in l]
		else:
			return [x if isinstance(x,list) and len(x) == 2 else [x[0],x[0]] if isinstance(x,list) else [x,x] for x in l]

	decoder_kernel_sizes = list(reversed(kernel_sizes))
	decoder_strides = list(reversed(strides))

	encoder = models.ConvolutionalEncoder(
		encoder_input_size,
		_make_2d(kernel_sizes,model_class),
		_make_2d(strides, model_class),
		filter_channels,
		encoder_output_size,
		bottleneck_activation,
		depthwise_conv)

	decoder_paddings = list(reversed(encoder.paddings))
	decoder_shapes = list(reversed(encoder.output_sizes)) + [encoder_input_size]

	decoder = models.ConvolutionalDecoder(
		decoder_input_size,
		decoder_shapes,
		_make_2d(decoder_kernel_sizes, model_class),
		_make_2d(decoder_strides, model_class),
		decoder_paddings,
		final_activation,
		depthwise_conv)

	return getattr(models,autoencoder_class)(encoder,decoder,**kwargs)
