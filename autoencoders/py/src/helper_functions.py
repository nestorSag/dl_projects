#Understanding text data with deep learning
import re
import os
import pandas as pd
import numpy as np 
import torch
import torch.optim as optim

import scipy.sparse as sparse

np.random.seed(1)

### batch -> vectorised distance matrix (only upper half)
def get_diff_matrix(batch,diff_type="correlation"):
	# get vectorised upper diagonal of distance matrix 
	n = batch.shape[0]
	diff = torch.empty((int(n*(n-1)/2),))
	k = 0
	#normalise batch
	for i in range(n-1):
		for j in range(i+1,n):
			if diff_type == "distance":
				diff[k] = ((batch[i,:] - batch[j,:])**2).mean() #Euclidean distance
			elif diff_type == "correlation":
				norm_i = ((batch[i,:]**2).sum())**(0.5)
				norm_j = ((batch[j,:]**2).sum())**(0.5)
				#
				diff[k] = batch[i,:].dot(batch[j,:])/(norm_i*norm_j)
			elif diff_type == "dot product":
				diff[k] = batch[i,:].dot(batch[j,:])/n #dot product
			else:
				raise Exception("difference metric not understood")
			k += 1
	#
	return diff

def get_unit_sphere_projection(data,mean,sdev):
	z = (data - mean)/sdev
	z_norms = np.sqrt(np.sum(np.abs(z)**2,axis=-1))
	return z/z_norms.reshape(-1,1)

def angular_trajectory(model,n_points=100):
	# for angular autoencoder only and in R3 only. Returns dataframe with closed path coordinates
	#
	t = model.decoder.forward(torch.Tensor([x/n_points for x in range(n_points+1)]).view(-1,1))
	z = model._angular_to_cartesian(t).detach().numpy()
	df = pd.DataFrame(z)
	df.columns = ["x","y","z"]
	df["rank"] = range(df.shape[0])
	return df
	#

def sphere_coords(theta, phi): 
	# get sphere coordinates to plot with plotly's Surface
    x=np.cos(phi)*np.cos(theta)
    y=np.cos(phi)*np.sin(theta)
    z=np.sin(phi)  
    return x,y,z