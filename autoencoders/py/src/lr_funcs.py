
def angular_reuters_lr_lambda(t):
	return 1 if t < 20 else 1e-1

def angular_mnist_lr_lambda(t):
	return 1 if t < 1000 else 1e-1#if t < 1000 else 0.5*1e-1# if t < 3500 else 1e-2

def angular_hand_posture_lr_lambda(t):
	return 1e-1 if t < 1000 else 1e-2#if t < 1000 else 0.5*1e-1# if t < 3500 else 1e-2

def angular_legal_data_lr_lambda(t):
	return 1e-1 if t < 1000 else 1e-2#if t < 1000 else 0.5*1e-1# if t < 3500 else 1e-2

def conv_mnist_lr_lambda(t):
	return 1 if t < 50 else 1e-1 if t < 100 else 1e-2
