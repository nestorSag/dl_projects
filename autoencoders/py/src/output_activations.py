import torch

def IAE_mnist_output_activation(x):
	return 255*torch.sigmoid(x)
