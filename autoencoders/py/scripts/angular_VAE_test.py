import sys
import os
import json
import pandas as pd
import numpy as np

import torch
import torch.optim as optim

import sys
sys.path.append('autoencoders/py/src')
import data_getters as data_getters
import model_getters as model_getters
import lr_funcs as lr_funcs

import matplotlib.pyplot as plt
import seaborn as sns

config_file = "autoencoders/py/experiments/configs/angular_mnist_WBN.json"

np.random.seed(1)
torch.manual_seed(1)

with open(config_file,"r") as f:
	config = json.loads(f.read())

# get data
get_data = getattr(data_getters,config["data"]["getter"])
data_list = get_data()
data, mu, sigma = data_list[0:3] #sometimes data getters return additional info
data_dim = data.shape[1]


# get model
model_params = config["model"]["getter_params"]
model_params["input_size"] = data_dim
model = getattr(model_getters,config["model"]["getter"])(**model_params)

# get opt

opt = getattr(optim,config["opt"]["class"])(model.parameters(),**config["opt"]["params"])

if "scheduler" not in config.keys() or config["scheduler"] is None:
	scheduler = None
else:
	func = getattr(lr_funcs,config["scheduler"]["params"]["lr_lambda"])
	scheduler = getattr(optim.lr_scheduler,config["scheduler"]["class"])(opt,func)

#train model
print("Fitting model...")
model.train(
	opt,
	data,
	mu,
	sigma,
	config["training"]["batch_size"],
	config["training"]["epochs"],
	scheduler,
	True)


x,y = model.project(data,mu,sigma)
#sns.scatterplot(x,y,hue=["_" + str(x) for x in list(data_list[-1])])
sns.scatterplot(x,y)
plt.show()
#plt.close()

pd.DataFrame({"loss":model.loss_trace}).to_csv(output_folder + "loss_trace.csv")
pd.DataFrame({"lr":model.lr}).to_csv(output_folder + "lr.csv")

model.plot_loss()

output_folder = "autoencoders/py/experiments/results/angular_legal_data_WBN/"
if not os.path.exists(output_folder):
	os.mkdir(output_folder)

torch.save(model,output_folder + "model")

x,y = model.project(data,mu,sigma)
projection_df = pd.DataFrame({"x":x,"y":y}).to_csv(output_folder + "projection.csv")
sns.scatterplot(x,y,alpha=0.3)
plt.savefig(output_folder + "projection.png")
#plt.show()
plt.close()

pd.DataFrame({"loss":model.loss_trace}).to_csv(output_folder + "loss_trace.csv")

model.plot_loss(output_folder + "loss_trace.png",show=False)

config["source_config"] = config_file
with open(output_folder + "config.json","w") as f:
	f.write(json.dumps(config))
