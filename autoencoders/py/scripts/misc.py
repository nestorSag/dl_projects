#Understanding text data with deep learning
import re
import os
import math
import numpy as np 
import torch
import torch.optim as optim

from rlmodels.nets import FullyConnected

import plotly.express as px
import plotly.graph_objects as go

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

import sys
sys.path.append('autoencoders/py/src')
import models as models
import data_getters as getters
import lr_funcs as lr_funcs
import helper_functions as hf



#### create 3D sphere GIF
k = 5
n_k = 100
s = 15
gmm_data, gmm_mu, gmm_sigma = getters.get_gmm_data(k,n_k,s)
z = get_unit_sphere_projection(gmm_data,gmm_mu,gmm_sigma)
data_df = pd.DataFrame(z)
data_df.columns = ["x","y","z"]

torch.manual_seed(1)

## encoder1 setup
data_dim = gmm_data.shape[1]
nn_shape = [50,50]
lr = 1e-1
epochs = 1500
mb_size = 50

model = models.AngularAutoencoder(nn_shape,nn_shape,data_dim)

opt = optim.SGD(model.parameters(),lr=lr,weight_decay = 0, momentum = 0.3)
scheduler = optim.lr_scheduler.LambdaLR(opt,lambda t: 1 if t < 100 else 1e-1)

model.train(opt,gmm_data,gmm_mu,gmm_sigma,mb_size,epochs,scheduler)

#model.plot_loss()
#model.project(gmm_data,gmm_mu,gmm_sigma)
#model.plot_closedness_loss()

# plot sphere in R3
theta=np.linspace(0,2*np.pi,40)
phi=np.linspace(-np.pi/2, np.pi/2, 30)
theta,phi=np.meshgrid(theta,phi)
x,y,z=hf.sphere_coords(theta,phi)

sphere = go.Surface(
        z=z, 
        x=x,  
        y=y,
        colorscale='Viridis')

model_df = hf.angular_trajectory(model)

fig = go.Figure([sphere])
fig = fig.add_scatter3d(
	x=data_df['x'], 
	y=data_df['y'],
	z=data_df['z'],
	marker=dict(color='#CC00CC',size=5)
)

fig = fig.add_scatter3d(
	x=model_df['x'], 
	y=model_df['y'], 
	z=model_df['z'],
	mode="lines+markers",
	marker=dict(color='#FF8000',size=5,line = dict(color='#FF8000',width=25)))

fig.show()

#### create hand posture  blog figure 

projection_df = pd.read_csv("autoencoders/py/experiments/results/cartesian_hand_posture_WBN/projection.csv")[["x","y"]]
loss_df = pd.read_csv("autoencoders/py/experiments/results/cartesian_hand_posture_WBN/loss_trace.csv")
loss_df["epoch"] = list(range(1,loss_df.shape[0]+1))

#model = torch.load("autoencoders/py/experiments/results/angular_hand_posture_WBN_ciclical/model")

sample_df = projection_df.sample(n=min(3000,projection_df.shape[0]))
plt.subplot(121)
sns.scatterplot(data=sample_df,x="x",y="y",size=0.3,legend=False,color="purple")
plt.subplot(122)
sns.lineplot(data=loss_df.query("epoch > 1"),x="epoch",y="loss",c="purple")
plt.tight_layout()
plt.show()


loss_df = pd.DataFrame({"loss":model.loss_trace,"epoch": range(len(model.loss_trace))})
projection_df = pd.DataFrame({"x":x,"y": y})
plt.subplot(121)
plt.hexbin(projection_df["x"],projection_df["y"])
plt.subplot(122)
sns.lineplot(data=loss_df,x="epoch",y="loss")
plt.tight_layout()
plt.show()


plt.hexbin(x,y)
