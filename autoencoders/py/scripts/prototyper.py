import sys
import os
import json
import pandas as pd
import numpy as np

import torch
import torch.optim as optim

import sys
sys.path.append('autoencoders/py/src')
import data_getters as data_getters
import model_getters as model_getters
import lr_funcs as lr_funcs

import matplotlib.pyplot as plt
import seaborn as sns

MAX_SCATTERPLOT_N = 3000
filename = "angular_conv_mnist"
config_file = "autoencoders/py/experiments/configs/" + filename + ".json"

np.random.seed(1)
torch.manual_seed(1)

with open(config_file,"r") as f:
	config = json.loads(f.read())

# get data
get_data = getattr(data_getters,config["data"]["getter"])
data_list = get_data()
data, mu, sigma = data_list[0:3] #sometimes data getters return additional info
data_dim = data.shape[1::]
data_dim = int(data_dim[0]) if len(data_dim) == 1 else data_dim
#data = torch.Tensor(data)

#mu = torch.Tensor(mu).view(data_dim)
#sigma = torch.Tensor(sigma).view(data_dim)


# get model
model_params = config["model"]["getter_params"]
model_params["input_size"] = data_dim
model = getattr(model_getters,config["model"]["getter"])(**model_params)

# get opt

opt = getattr(optim,config["opt"]["class"])(model.parameters(),**config["opt"]["params"])

if "scheduler" not in config.keys() or config["scheduler"] is None:
	scheduler = None
else:
	func = getattr(lr_funcs,config["scheduler"]["params"]["lr_lambda"])
	scheduler = getattr(optim.lr_scheduler,config["scheduler"]["class"])(opt,func)

#train model
print("Fitting model...")
model.fit(
	opt,
	data,
	mu,
	sigma,
	config["training"]["batch_size"],
	config["training"]["epochs"],
	scheduler,
	True)


#projection = model.project(data,mu,sigma)
projection = model.encode(data,mu,sigma).detach().numpy()
x = projection[:,0]
y = projection[:,1]
sns.scatterplot(x,y,hue=["a" + str(x) for x in list(data_list[-1])],size=0.3,legend=False)
#sns.scatterplot(x,y,size=0.3,legend=False)
plt.show()
#plt.close()

model.plot_loss()

output_folder = "autoencoders/py/experiments/results/" + filename + "/"
if not os.path.exists(output_folder):
	os.mkdir(output_folder)

torch.save(model,output_folder + "model")

projection = model.project(data,mu,sigma)
x = projection[:,0]
y = projection[:,1]
pd.DataFrame({"x":x,"y":y}).to_csv(output_folder + "projection.csv")

if x.shape[0] > MAX_SCATTERPLOT_N:
	idx = np.random.choice(x.shape[0],MAX_SCATTERPLOT_N,False)
	x = x[idx]
	y = y[idx]

idx1 = (x >= np.quantile(x,0.025))*(x <= np.quantile(x,0.975))
idx2 = (y >= np.quantile(y,0.025))*(y <= np.quantile(y,0.975))
idx = idx1*idx2

sns.scatterplot(x[idx],y[idx])
plt.savefig(output_folder + "projection.png")
plt.close()

plt.hexbin(x[idx],y[idx],gridsize=40)
plt.savefig(output_folder + "hexbin_projection.png")
plt.close()

pd.DataFrame({"loss":model.loss_trace}).to_csv(output_folder + "loss_trace.csv")

model.plot_loss(output_folder + "loss_trace.png",show=False)

config["source_config"] = config_file
with open(output_folder + "config.json","w") as f:
	f.write(json.dumps(config))
