import sys
import os
import json
import pandas as pd
import numpy as np

import torch
import torch.optim as optim

import sys
sys.path.append('autoencoders/py/src')
import models as models
import data_getters as getters
import lr_funcs as lr_funcs

import matplotlib.pyplot as plt
import seaborn as sns

config_file = "autoencoders/py/experiments/configs/CAE_mnist.json"

np.random.seed(1)
torch.manual_seed(1)

with open(config_file,"r") as f:
	config = json.loads(f.read())


get_data = getattr(getters,config["data"]["getter"])
data_list = get_data(n=1000)
data, mu, sigma = data_list[0:3] #sometimes data getters return additional info
data = torch.Tensor(data)

data_dim = data.shape[1::]

mu = torch.Tensor(mu).view(data_dim)
sigma = torch.Tensor(sigma).view(data_dim)

#set model
model_class = getattr(models,config["model"]["class"])

model = model_class(
	input_dim = data_dim,
	kernel_sizes = config["model"]["kernel_sizes"],
	strides = config["model"]["strides"],
	filter_channels = config["model"]["filter_channels"],
	encoding_dim = config["model"]["encoding_dim"],
	bottleneck_activation = config["model"]["bottleneck_activation"],
	final_activation = config["model"]["final_activation"],
	depthwise_conv = False)


# set optimizer
# opt = optim.SGD(
# 	model.parameters(),
# 	lr=config["opt"]["lr"],
# 	weight_decay = config["opt"]["weight_decay"],
# 	momentum = config["opt"]["momentum"])

# if config["scheduler"]["class"] == "LambdaLR":
# 	func = getattr(lr_funcs,config["scheduler"]["params"]["lr_lambda"])
# 	scheduler = getattr(optim.lr_scheduler,config["scheduler"]["class"])(
# 		opt,
# 		lr_lambda = func)
# else:
# 	scheduler = getattr(optim.lr_scheduler,config["scheduler"]["class"])(
# 		opt,
# 		**config["scheduler"]["params"])

opt = optim.Adam(
	model.parameters(),
	lr = 0.0005)
#train model
print("Fitting model...")
model.train(
	opt,
	data,
	mu,
	sigma,
	config["training"]["batch_size"],
	config["training"]["epochs"],
	scheduler,
	debug=False)


x,y = model.project(data,mu,sigma)
#sns.scatterplot(x,y,alpha=0.3)
sns.scatterplot(x,y,hue =['a' + str(x) for x in data_list[-1]])
plt.show()
#plt.close()



sample = (model.decoder.forward(torch.rand((1,config["model"]["encoding_dim"])))*sigma + mu).\
detach().\
numpy().\
astype(np.int32).\
clip(min=0,max=255).\
reshape(data_dim[1::])

plt.imshow(sample)
plt.show()

n_bins = 30
a = plt.hexbin(x,y, gridsize=n_bins, cmap='viridis')
plt.show()

pd.DataFrame({"loss":model.loss_trace}).to_csv(output_folder + "loss_trace.csv")
pd.DataFrame({"lr":model.lr}).to_csv(output_folder + "lr.csv")

model.plot_loss()



output_folder = "autoencoders/py/experiments/results/angular_hand_posture/"
if not os.path.exists(output_folder):
	os.mkdir(output_folder)
torch.save(model,output_folder + "model")

x,y = model.project(data,mu,sigma)
projection_df = pd.DataFrame({"x":x,"y":y}).to_csv(output_folder + "projection.csv")
sns.scatterplot(x,y,alpha=0.3)
plt.savefig(output_folder + "projection.png")
#plt.show()
plt.close()

pd.DataFrame({"loss":model.loss_trace}).to_csv(output_folder + "loss_trace.csv")
pd.DataFrame({"lr":model.lr}).to_csv(output_folder + "lr.csv")

model.plot_loss(output_folder + "loss_trace.png",show=False)

config["source_config"] = config_file
with open(output_folder + "config.json","w") as f:
	f.write(json.dumps(config))
