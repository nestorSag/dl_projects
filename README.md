# Deep learning sandbox

I use this repository to play around with deep learning models. Mostly to post interesting findings on my website: www.nestorsag.com

## Installing

Clone the repository and download dependencies with `pipenv`:

```
pip install pipenv
pipenv install
```

The scripts should run from the root folder after that.